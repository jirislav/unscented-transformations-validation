# About this project

The aim of the project is to design and implement the algorithm for the validation 
of the estimation of the momentary characteristics of the random variable, 
which represents the apostricular distribution of the probability estimation 
of the parameters of the general nonlinear estimation problem solved by the method 
of maximal credibility. 

Validation is performed by comparing a validated estimate with an estimate made using Unscented Transformation.

*NOTE: it is recommended to view this README using standard markdown interpreter 
(e.g. online at [GitLab.com/jirislav/unscented-transformations-validation](https://gitlab.com/jirislav/unscented-transformations-validation) - use can also browse code there)* 

## The goals
 - design
 - implementation
 - verification

## Possible software
 - MATLAB / Octave / R / C / C++ / C# / Python / Java / ...

## Project outputs
 - implementation
 - PDF documentation (tex)

# Implementation

During the implementation it is necessary to perform square root of a matrix.
There are several approaches to perform this task, but we will focus on using the [Cholesky decomposition](https://en.wikipedia.org/wiki/Cholesky_decomposition).

Great introduction into Unscented Transformation computation using Python can be found [here](http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb)

## How to run the program

```bash
# Python 3.6 and higher is required
apt install python3.6 python3-pip
# project dependencies are managed using venv, but you may install them using pip3 if you want 
pip3 install venv
# Use the Virtual environment for current terminal session (it may be populated using PyCharm for example)
source venv/bin/activate

# And finally, run the script
cd src
./unscented_transformation.py
```
### Program inputs
 - multidimensional probability distribution function
 - momentary characteristics

### Program outputs
 - validation of the provided momentary characteristics using the [Unscented Transformation](https://en.wikipedia.org/wiki/Unscented_transform).


### Program interface
 - shell / interactive console
 - graphs / figures only for one/two-dimensional functions with momentary characteristics

# Graphical assignments:

![](gallery/assignment.jpg)
![](gallery/2nd_assignment.jpg)
![](gallery/3rd_assignment.jpg)

# 2D functions with moments

![](gallery/graphs/Gaussian_distribution_with_moments.png)
![](gallery/graphs/Custom1_2D_function_with_moments.png)

# 3D functions with moments

![](gallery/graphs/3D_gaussian_distribution_with_moments.png)
![](gallery/graphs/Custom1_3D_function_with_moments.png)
