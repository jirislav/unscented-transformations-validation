from typing import Callable, Union, Tuple, Type

import numpy as np
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
from matplotlib.scale import ScaleBase
from mpl_toolkits.mplot3d import Axes3D
from numpy.core.multiarray import ndarray
import matplotlib.pyplot as plt
from matplotlib import rcParams

# Don't remove this import, it's presence is mandatory
# noinspection PyUnresolvedReferences
from mpl_toolkits import mplot3d

from lib.stats import SigmaPointsContainer

_LATEX_PACKAGES = [
    'amsmath'
]

# Comment this out if causing problems ..
rcParams['text.usetex'] = True
for pkg in _LATEX_PACKAGES:
    rcParams['text.latex.preamble'].append(r'\usepackage{{{}}}'.format(pkg))


def standard_plot(
        x_values: Union[ndarray, Tuple[ndarray, ndarray]],
        y_values: ndarray,
        title: str = None,
        window_title: str = None,
        window_width_multiplier: float = None,
        window_height_multiplier: float = None,
        x_label: str = None,
        y_label: str = None,
        y_min: float = -np.Inf,
        y_max: float = np.Inf,
        x_scale: Union[str, Type[ScaleBase], Tuple[Type[ScaleBase], Type[ScaleBase]], Tuple[str, str]] = None,
        y_scale: str = None,
        do_not_show_yet: bool = False,
        block: bool = False,
        figure: Figure = None,
        show_grid: bool = False,
        show_axes: bool = True,
        debug: bool = False,
) -> Figure:
    max_points_to_plot = 3e7
    if y_values.size > max_points_to_plot:
        raise Exception(
            "ERROR: I'm not gonna kill the system by running our of RAM!\n"
            "        You requested to plot {} points, but {} is the maximum!".format(
                y_values.size, max_points_to_plot
            )
        )

    if debug:
        print('DEBUG: Creating plot with {} points ... '.format(y_values.size), end='', flush=True)

    _3d = False
    if not isinstance(x_values, ndarray):
        _3d = True

    if y_min != -np.Inf:
        y_values[y_values < y_min] = y_min

    if y_max != np.Inf:
        y_values[y_values > y_max] = y_max

    if not figure:
        figure = plt.figure()

        # When first creating figure, widen it by 30 % (if not requested otherwise)
        if not window_width_multiplier:
            window_width_multiplier = 1.3
    else:
        # Activate provided figure ..
        figure = plt.figure(figure.number)

    if window_width_multiplier:
        figure.set_figwidth(figure.get_figwidth() * window_width_multiplier)

    if window_height_multiplier:
        figure.set_figheight(figure.get_figheight() * window_height_multiplier)

    if not _3d:
        plt.plot(x_values, y_values)

        if not x_label:
            x_label = 'Axis X'
        if not y_label:
            y_label = 'Axis Y'

        plt.xlabel(x_label)
        plt.ylabel(y_label)

        if x_scale:
            plt.xscale(x_scale)

        if y_scale:
            plt.yscale(y_scale)

        if show_grid:
            plt.grid(True, which='both')

        if show_axes:
            # Line2D
            plt.axhline(y=0, color='gray', linestyle='-.', linewidth=0.75)
            plt.axvline(x=0, color='gray', linestyle='-.', linewidth=0.75)
    else:
        ax: Axes3D = plt.axes(projection='3d')
        ax.plot_surface(
            X=x_values[0],
            Y=x_values[1],
            Z=y_values,
            alpha=.8
        )

        if not (isinstance(x_label, tuple) and len(x_label) == 2):
            x_label = ('Axis X', 'Axis Y')

        if not y_label:
            y_label = 'Axis Z'

        ax.set_xlabel(x_label[0])
        ax.set_ylabel(x_label[1])
        ax.set_zlabel(y_label)

        if isinstance(x_scale, tuple) and len(x_scale) == 2:
            ax.set_xscale(x_scale[0])
            ax.set_yscale(x_scale[1])

        if y_scale:
            ax.set_zscale(y_scale)

    if title:
        plt.title(title, fontsize=20, y=1.05)

    if debug:
        print('done')

    if not do_not_show_yet:
        plt.show(block=block)

    plt.subplots_adjust(left=0.1, top=0.8, right=0.9)

    plt.tight_layout()

    if window_title:
        canvas: FigureCanvasTkAgg = figure.canvas
        canvas.set_window_title(window_title)

    # noinspection PyUnboundLocalVariable
    return figure if not _3d else (figure, ax)


def plot_sigmas(
        sigma_points: SigmaPointsContainer,
        fn,
        figure: Figure = None,
        ax: Axes3D = None,
        block: bool = False,
        do_not_show_yet: bool = False,
) -> Figure:

    if not figure:
        figure = plt.figure()
    else:
        # Activate provided figure ..
        figure = plt.figure(figure.number)

    dimensionality = sigma_points.moments.means.size

    if dimensionality == 2:
        plt.scatter(
            x=sigma_points.sigmas[:, 0],
            y=fn(sigma_points.sigmas[:, 0]),
            c='green'
        )
    elif dimensionality == 3:
        if not ax:
            ax: Axes3D = plt.axes(projection='3d')

        ax.scatter(
            xs=sigma_points.sigmas[:, 0],
            ys=sigma_points.sigmas[:, 1],
            zs=fn(sigma_points.sigmas[:, 0], sigma_points.sigmas[:, 1]),
            s=300,
            c='green'
        )
    else:
        print('WARNING: Cannot plot 4D sigmas yet ... sorry')
        return figure

    if not do_not_show_yet:
        plt.show(block=block)

    return figure


def calculate_and_plot(
        func: Callable[[ndarray], ndarray],
        x_values: ndarray,
        **kwargs
) -> Figure:
    return standard_plot(x_values=x_values, y_values=func(x_values), **kwargs)
