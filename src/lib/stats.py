from typing import Sequence, Callable

import numpy as np
# noinspection PyProtectedMember
from filterpy.common import pretty_str
from scipy.linalg import cholesky


class Moments:

    __MAX_DIMENSIONALITY: int = 10

    dimensions: int
    means: np.ndarray
    covariance_matrix: np.matrix

    def __init__(self, function_inputs: Sequence[np.ndarray], function_outputs: np.ndarray, debug: bool = False):
        """
            Calculates basic moments

        :param function_inputs:
        :param function_outputs:
        """

        if debug:
            print("DEBUG:: Calculating moments ... ", end='', flush=True)

        dimension_length = self.validate(function_inputs, function_outputs)

        input_dimensions_flattened = np.empty((len(function_inputs), dimension_length))
        for i, input_dimension in enumerate(function_inputs):
            input_dimensions_flattened[i] = input_dimension.flatten()

        output_dimension_flattened = function_outputs.flatten()

        self.means = np.array([
            np.mean(dimension) for dimension in (*input_dimensions_flattened, output_dimension_flattened)
        ])

        self.covariance_matrix = np.cov(input_dimensions_flattened)

        if not self.covariance_matrix.shape:
            # fix the shape of a single valued matrix
            _matrix = np.empty((1, 1))
            _matrix[0, 0] = self.covariance_matrix
            self.covariance_matrix = _matrix

        if debug:
            print('done (\n\tmeans={}\n\tcovariance_matrix={}\n)'.format(self.means, self.covariance_matrix))

    @staticmethod
    def validate(x: Sequence[np.ndarray], y: np.ndarray) -> int:

        if len(x) + 1 > Moments.__MAX_DIMENSIONALITY:
            raise Exception('You can provide up to {} dimension, but you have provided {}!'.format(
                Moments.__MAX_DIMENSIONALITY,
                len(x) + 1
            ))

        dimensions_length = x[0].size

        if not dimensions_length > 0:
            raise Exception('You have provided empty dimension!')

        if not all(dimension.size == dimensions_length for dimension in (*x, y)):
            raise Exception('All dimensions must be of the same length!')

        return dimensions_length


class SigmaPointsContainer:

    moments: Moments
    n: int
    alpha: float
    beta: float
    kappa: float

    sigmas: np.ndarray

    def __init__(self, moments: Moments, alpha: float = None, beta: float = None, kappa: float = None,
                 sqrt_method: Callable = cholesky, subtract_method: Callable = np.subtract):
        self.moments = moments
        self.n = max(moments.covariance_matrix.shape)

        self.alpha = .3 if not alpha else alpha
        self.beta = 2. if not beta else beta
        self.kappa = 3. - self.n if not kappa else kappa

        if not 0 < self.alpha < 1:
            print('WARNING!')
            print(' - α coefficient should have value between <0,1> - it has {:.1f} instead'.format(self.alpha))
            print('- see the discussion here: http://nbviewer.jupyter.org/github/rlabbe/Kalman-and-Bayesian-Filters'
                  '-in-Python/blob/master/10-Unscented-Kalman-Filter.ipynb#Reasonable-Choices-for-the-Parameters')

        self.sqrt = sqrt_method
        self.subtract = subtract_method

        # In our use case this is unnecessary, but one never knows ..
        self._compute_weights()

        # create sigma points and weights
        self.sigmas = self.sigma_points(self.moments.means[:-1], self.moments.covariance_matrix)

    def num_sigmas(self):
        """ Number of sigma points for each variable in the state x"""
        return 2 * self.n + 1

    def sigma_points(self, means, covariance):

        if self.n != np.size(means):
            raise ValueError("expected size(means) {}, but size is {}".format(
                self.n, np.size(means)))

        n = self.n

        if np.isscalar(means):
            means = np.asarray([means])

        if  np.isscalar(covariance):
            covariance = np.eye(n) * covariance
        else:
            covariance = np.atleast_2d(covariance)

        lambda_ = self.alpha**2 * (n + self.kappa) - n
        U = self.sqrt((lambda_ + n) * covariance)

        sigmas = np.zeros((2*n+1, n))
        sigmas[0] = means
        for k in range(n):
            # pylint: disable=bad-whitespace
            sigmas[k+1]   = self.subtract(means, -U[k])
            sigmas[n+k+1] = self.subtract(means, U[k])

        return sigmas

    def _compute_weights(self):

        n = self.n
        lambda_ = self.alpha**2 * (n + self.kappa) - n

        c = .5 / (n + lambda_)
        self.Wc = np.full(2*n + 1, c)
        self.Wm = np.full(2*n + 1, c)
        self.Wc[0] = lambda_ / (n + lambda_) + (1 - self.alpha**2 + self.beta)
        self.Wm[0] = lambda_ / (n + lambda_)

    def __repr__(self):

        return '\n'.join([
            'MerweScaledSigmaPoints object',
            pretty_str('n', self.n),
            pretty_str('alpha', self.alpha),
            pretty_str('beta', self.beta),
            pretty_str('kappa', self.kappa),
            pretty_str('Wm', self.Wm),
            pretty_str('Wc', self.Wc),
            pretty_str('subtract', self.subtract),
            pretty_str('sqrt', self.sqrt)
            ])
