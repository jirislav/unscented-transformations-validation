from numbers import Number
from typing import Union, Iterable, Callable

import numpy as np
import scipy.signal
from numpy.core.multiarray import ndarray


def gaussian(x: Union[Number, ndarray, Iterable], order: int = 0) -> Union[Number, ndarray, Iterable]:
    return scipy.signal.gauss_spline(x=x, n=order)


def exponential(x: Union[Number, ndarray, Iterable]) -> Union[Number, ndarray, Iterable]:
    return np.exp(x)


def inverse_2d(x_1: Union[Number, ndarray, Iterable], x_2: Union[Number, ndarray, Iterable]) -> Union[Number, ndarray, Iterable]:
    return np.power(x_1 * x_2, -1)


def inverse(x: Union[Number, ndarray, Iterable], max: float = np.Inf) -> Union[Number, ndarray, Iterable]:

    _inverse = np.power(x, -1)

    if max != np.Inf:
        _inverse[_inverse > max] = max
        _inverse[_inverse < -max] = -max

    return _inverse


def construct_gaussian(k: float = 1.0, c: float = 0.0, order: int = 1) -> Callable[
    [
        Union[Number, ndarray, Iterable],
    ],
    Union[Number, ndarray, Iterable]
]:
    """
        Returns wrapper function for calling gaussian function, accepting only X input - as follows:
        gaussian( x=k * X + c, order)

    :return:
    """
    if k == 0.0:
        return lambda: gaussian(x=c, order=order)

    return lambda x: gaussian(x=k * x + c, order=order)


def construct_inverse(k: float = 1.0, c: float = 0.0) -> Callable[
    [
        Union[Number, ndarray, Iterable],
    ],
    Union[Number, ndarray, Iterable]
]:
    """
        Returns function accepting input X and calculating Y:
        Y = 1 / ( k * X + c )

    :return:
    """
    if k == 0.0:
        return lambda: inverse(c)

    return lambda x: inverse(k * x + c)
