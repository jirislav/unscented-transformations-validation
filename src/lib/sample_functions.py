from typing import Callable, Tuple

import numpy as np
from matplotlib.axes import Axes
from matplotlib.figure import Figure

from lib.formatting import format_equation_with_moments
from lib.non_linear_functions import gaussian
from lib.plot import standard_plot
from lib.stats import Moments


def gaussian_2d(block: bool = False, debug: bool = False) -> Tuple[Callable, Moments, Figure]:
    start = -1
    end = 1
    step = 1e-5

    def fn(x):
        return gaussian(x, 0)

    x = np.arange(start, end + step, step)
    y = fn(x)

    moments = Moments(function_inputs=(x,), function_outputs=y, debug=debug)

    fig = standard_plot(
        x_values=x,
        y_values=y,
        block=block,
        title=format_equation_with_moments(
            tex_equation=r'y = \frac{1}{\sqrt{2 \pi}} e^{-\frac{1}{2} x^2}',
            moments=moments
        ),
        window_title='Gaussian distribution with moments',
        window_width_multiplier=1.4,
        debug=debug
    )

    return fn, moments, fig


def gaussian_3d(block: bool = False, debug: bool = False) -> Tuple[Callable, Moments, Figure, Axes]:
    start = -1
    end = 1
    step = 1e-3
    x_range = np.arange(start, end + step, step)

    def fn(x, y):
        return gaussian(np.sqrt(x**2 + y**2), 0)

    x, y = np.meshgrid(x_range, x_range)
    z = fn(x, y)

    moments = Moments(function_inputs=(x, y), function_outputs=z, debug=debug)

    fig, ax = standard_plot(
        x_values=(x, y),
        y_values=z,
        block=block,
        title=format_equation_with_moments(
            tex_equation=r'z = \frac{1}{\sqrt{2 \pi}} e^{-\frac{1}{2} (x^2 + y^2)}',
            moments=moments
        ),
        window_title='3D gaussian distribution with moments',
        window_width_multiplier=1.6,
        window_height_multiplier=1.3,
        debug=debug
    )

    return fn, moments, fig, ax


def custom1_2d(block: bool = False, debug: bool = False) -> Tuple[Callable, Moments, Figure]:
    start = -1.5
    end = 1.5
    step = 1e-4

    def fn(x: np.ndarray):
        return abs(np.sin(5.5*np.pi*(x+1/3))) * gaussian(x, 0)

    x = np.arange(start, end + step, step)
    y = fn(x)

    moments = Moments(function_inputs=(x,), function_outputs=y, debug=debug)

    fig = standard_plot(
        x_values=x,
        y_values=y,
        block=block,
        title=format_equation_with_moments(
            tex_equation=r'y = \lvert sin(\frac{11}{2} \pi (x + \frac{1}{3})) \rvert \frac{1}{\sqrt{2 \pi}} e^{-\frac{1}{2} x^2}',
            moments=moments
        ),
        window_title='Custom1 2D function with moments',
        window_width_multiplier=1.4,
        debug=debug
    )

    return fn, moments, fig


def custom1_3d(block: bool = False, debug: bool = False) -> Tuple[Callable, Moments, Figure, Axes]:
    start = -1
    end = 1
    step = 1e-3
    x_range = np.arange(start, end + step, step)

    def fn(x: np.ndarray, y: np.ndarray):
        return abs(np.sin(5.5*np.pi*(x+1/3))) * gaussian(np.sqrt(x**2 + y**2), 0)

    x, y = np.meshgrid(x_range, x_range)
    z = fn(x, y)

    moments = Moments(function_inputs=(x, y), function_outputs=z, debug=debug)

    fig, ax = standard_plot(
        x_values=(x, y),
        y_values=z,
        block=block,
        title=format_equation_with_moments(
            tex_equation=r'y = \lvert sin(\frac{11}{2} \pi (x + \frac{1}{3})) \rvert \frac{1}{\sqrt{2 \pi}} e^{-\frac{1}{2} (x^2 + y^2)}',
            moments=moments
        ),
        window_title='Custom1 3D function with moments',
        window_width_multiplier=1.6,
        window_height_multiplier=1.3,
        debug=debug
    )

    return fn, moments, fig, ax