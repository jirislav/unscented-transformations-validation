import re
from collections import Iterable
from numbers import Number
from typing import Union

import numpy as np

from lib.stats import Moments


py_scientific = re.compile(r'(\d+)\.?(\d*)e\+?(-?)0?(.*)')


def format_equation_with_moments(
        tex_equation: str,
        moments: Moments
) -> str:
    return (
        r'${equation}$'
        r',~$\mu_{output_dim} = {means}$'
        r',~$\Sigma = {covariance}$'.format(
            equation=tex_equation,
            output_dim='z' if moments.means.size == 3 else 'y',
            means=number_2_tex_scientific(moments.means[-1]),
            covariance=matrix_2_tex_scientific(moments.covariance_matrix)
        )
    )


def number_2_tex_scientific(number: Union[Number, float], precision: int = 3, extremes_correction: bool = True) -> str:

    # Lets consider numbers smaller than 0.0000000000001 to be zero
    if extremes_correction:
        if number < 1e-13:
            return '0'
        if number > 1e13:
            return r'\infty'

    python_scientific_repr = '{{:.{}e}}'.format(precision).format(number)

    match = py_scientific.match(python_scientific_repr)
    if not match:
        return 'NaN'

    matches = match.groups('')

    # First group is a number preceding comma
    result = matches[0]

    # Second group is a number following comma
    if int(matches[1]) != 0:
        # Only print the decimal if there is something to show
        result = r','.join((result, matches[1]))

    # 4th group is the last integer of the exponent
    # if it is zero, don't use exponent at all (first exponent integer is guaranteed to also be 0 this way)
    if matches[3] != '0':
        result += r'\cdot10^{{{}{}}}'.format(*matches[2:])

    return result


def matrix_2_tex_scientific(matrix: Union[np.ndarray, Iterable]) -> str:
    return r'\\'.join([
        '&'.join(map(number_2_tex_scientific, line)) for line in matrix
    ]).join([
        r'\begin{bmatrix}',
        r'\end{bmatrix}'
    ])
