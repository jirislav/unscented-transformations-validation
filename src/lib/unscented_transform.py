from typing import Tuple, Callable

import numpy as np
from matplotlib.axes import Axes
from matplotlib.figure import Figure
from scipy.stats.stats import MannwhitneyuResult, mannwhitneyu

from lib.plot import plot_sigmas
from lib.stats import Moments, SigmaPointsContainer


def __normalize_weights(weights: np.ndarray) -> np.ndarray:
    """
        Normalized weights have their sum equal to 1.

    :param weights:
    :return:
    """
    return weights.copy() / sum(weights)


def verify(fn: Callable, moments: Moments, fig: Figure, ax: Axes = None, beta: float = 2.0, min_p_value: float = 0.01,
           debug: bool = False) -> bool:

    sigma = SigmaPointsContainer(moments=moments, beta=beta)
    plot_sigmas(sigma, fn, fig, ax)

    dimensionality = sigma.moments.means.size - 1

    custom_weights = fn(*(sigma.sigmas[:, dimension] for dimension in range(dimensionality)))
    ut_mean, ut_cov = unscented_transform(
        sigmas=sigma.sigmas,
        weights_mean=custom_weights,
        weights_covariance=custom_weights
    )

    if debug:
        print("== Provided Mean:       \n{}\n"
              "== Provided Covariance: \n{}\n"
              "== Computed Mean:       \n{}\n"
              "== Computed Covariance: \n{}\n"
              "== Diff Mean:  \n{}\n"
              "== Diff Covar: \n{}".format(
                  moments.means[:-1],
                  moments.covariance_matrix,
                  ut_mean,
                  ut_cov,
                  ut_mean - moments.means[:-1],
                  ut_cov - moments.covariance_matrix,
              ))

    if dimensionality == 1:
        covariance_from_ut = ut_cov
        covariance_computed = moments.covariance_matrix
    else:
        covariance_from_ut = ut_cov.flatten()
        covariance_computed = moments.covariance_matrix.flatten()

    mann_whitney_result: MannwhitneyuResult = mannwhitneyu(
        x=covariance_from_ut,
        y=covariance_computed,
        use_continuity=True,
        alternative='two-sided'
    )

    if debug:
        print(" ==> Mann Whitney U test results: \n{}".format(
            mann_whitney_result
        ))

    passed_verification = mann_whitney_result.pvalue >= min_p_value

    if debug:
        print(" Provided momentary characteristics {} to the provided function.".format(
            'BELONG' if passed_verification else "DOESN'T BELONG"
        ))

    return passed_verification


def unscented_transform(sigmas: np.ndarray, weights_mean: np.ndarray, weights_covariance: np.ndarray, noise_cov=None) \
      -> Tuple[np.ndarray, np.ndarray]:

    weights_mean = __normalize_weights(weights_mean)
    weights_covariance = __normalize_weights(weights_covariance)

    # new mean is just the sum of the sigmas * weight
    mean = np.dot(weights_mean, sigmas)    # dot = \Sigma^n_1 (W[k]*Xi[k])

    # new covariance is the sum of the outer product of the residuals
    # times the weights

    y = sigmas - mean[np.newaxis, :]
    covariance = np.dot(y.T, np.dot(np.diag(weights_covariance), y))

    if noise_cov is not None:
        covariance += noise_cov

    return mean, covariance
