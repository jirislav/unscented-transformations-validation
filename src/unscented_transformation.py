#!/usr/bin/env python3.6

from matplotlib import pyplot

from lib.sample_functions import custom1_2d, custom1_3d, gaussian_2d, gaussian_3d
from lib.unscented_transform import verify


# change this to true to turn on debugging info
debug = False

functions_to_verify = [
    custom1_2d,
    custom1_3d,
    gaussian_2d,
    gaussian_3d
]

for function_to_verify in functions_to_verify:
    arguments_for_verification = function_to_verify(debug=debug)
    verified: bool = verify(*arguments_for_verification, debug=debug)

    function_name = function_to_verify.__name__
    print("===============")
    if verified:
        print("SUCCESS: The moments we generated were successfully verified to belong to {} function".format(
            function_name
        ))
    else:
        print("FAILURE: Sorry, the generated moments cannot belong to {} function".format(function_name))

print("===============")

pyplot.show(block=True)
